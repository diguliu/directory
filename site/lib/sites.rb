require 'yaml'
require 'json'

def sites
  $sites ||= Dir.glob('content/sites/*/data.yaml').map do |f|
    data = YAML.load_file(f)
    id = File.basename(File.dirname(f))
    data['id'] = id
    data['screnshot'] = url_for("sites/#{id}/screenshot.png")
    data['thumbnail'] = url_for("sites/#{id}/screenshot.thumb.png")
    data
  end.sort_by { |site| site['id'] }
end

def site_id(data_file)
  File.basename(File.dirname(data_file))
end

def url_for(path)
  "http://directory.noosfero.org/#{path}"
end
